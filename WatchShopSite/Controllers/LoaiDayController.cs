﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WatchShopBUS.BUS;

namespace WatchShopSite.Controllers
{
    public class LoaiDayController : Controller
    {
        //
        // GET: /LoaiDay/

        public ActionResult dsLoaiDay()
        {
            return View(LoaiDayBUS.GetAll());
        }
        public ActionResult Detail(int id,int n=1)
        {
            var List = LoaiDayBUS.Detail(id,n);
            if (List.Items.Count()<=0)
            {
                return RedirectToAction("Index","Error");
            }
            return View(List);
        }
    }
}

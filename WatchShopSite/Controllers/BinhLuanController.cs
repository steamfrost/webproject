﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WatchShopBUS.BUS;
using WatchShopConnect;
using Microsoft.AspNet.Identity;

namespace WatchShopSite.Controllers
{
    public class BinhLuanController : Controller
    {
        //
        // GET: /BinhLuan/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult DSBinhLuan(string masp,int page=1)
        {
            return View(BinhLuanBUS.GetCommentUserView(masp,page));
        }
        public ActionResult ThemBinhLuan(BINHLUAN bl)
        {
            bl.THOIGIAN = DateTime.Now;
            if (bl.NGUOIBINHLUAN!="" && bl.THONGTINBINHLUAN !="")
                BinhLuanBUS.ThemBinhLuan(bl);
            return RedirectToAction("DSBinhLuan", new { masp = bl.MASANPHAM, page = 1 });
        }
	}
}
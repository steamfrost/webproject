﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WatchShopBUS.BUS;

namespace WatchShopSite.Controllers
{
    public class NhaSanXuatController : Controller
    {
        //
        // GET: /NhaSanXuat/

        public ActionResult DSNhaSanXuat()
        {
            return View(NhaSanXuatBUS.GetAllUserView());
        }
        [Route("/NhaSanXuat/Detail/{id}/{n}")]
        public ActionResult Detail(string id,int n=1)
        {
            var List = new PetaPoco.Page<WatchShopConnect.SP_VIEW>();
            var nsx = NhaSanXuatBUS.GetItemByID(id);
            List = NhaSanXuatBUS.GetDetail(id,n);
            if (List.Items.Count() <= 0)
            {
                return RedirectToAction("Index", "Error");
            }
            ViewBag.nsx = nsx.TENNHASANXUAT;
            return View(List);
        }
    }
}

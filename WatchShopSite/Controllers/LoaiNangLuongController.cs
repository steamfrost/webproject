﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WatchShopBUS.BUS;

namespace WatchShopSite.Controllers
{
    public class LoaiNangLuongController : Controller
    {
        //
        // GET: /LoaiNangLuong/

        public ActionResult dsLoaiNangLuong()
        {
            return View(LoaiNangLuongBUS.GetAll());
        }
        public ActionResult Detail(int id, int n = 1)
        {
            var List = LoaiNangLuongBUS.Detail(id,n);
            if (List.Items.Count() <= 0)
            {
                return RedirectToAction("Index", "Error");
            }
            return View(List);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WatchShopBUS.BUS;
using WatchShopBUS.Models;
using WatchShopConnect;

namespace WatchShopSite.Controllers
{
    public class SanPhamController : Controller
    {
        //
        // GET: /SanPham/

        public ActionResult Index(string id)
        {
            SANPHAM sp = SanPhamBUS.GetItemByID_Admin(id);
            sp.LUOTXEM += 1;
            sp.Save();
            var data = SanPhamBUS.GetItemByID(id);
            if (data.MASANPHAM == null || data.MASANPHAM=="")
            {
                return RedirectToAction("Index", "Error");
            }
            return View(data);
        }
        public ActionResult SanPhamCungNhaSanXuat(string mansx)
        {
            return View(SanPhamBUS.getItemByNSXID(mansx));
        }
        public ActionResult TimSanPhamTheoGia(int min,int max,int n=1)
        {
            ViewBag.min = min;
            ViewBag.max = max;
            return View(SanPhamBUS.TimSanPhamTheoGia(min, max, n));
        }
        /// <summary>
        /// Trả về chi tiết sản phẩm cho khách hàng
        /// </summary>
        /// <returns></returns>
        public ActionResult ViewDetail()
        {
            return View();
        }
        public ActionResult dsSPSale()
        {
            return View(SanPhamBUS.GetItemSale(10));
        }
        public ActionResult ChuongTrinhGiamGia(int n=1)
        {
            var List = SanPhamBUS.GetAllItemSale(n);
            return View(List);
        }
        public ActionResult FormTimKiem()
        {
            ViewBag.NSX = NhaSanXuatBUS.GetAll().Where(m=>m.DAXOA==false);
            ViewBag.LoaiVo = LoaiVoBUS.GetAll();
            ViewBag.LoaiDay = LoaiDayBUS.GetAll();
            ViewBag.LoaiNangLuong = LoaiNangLuongBUS.GetAll();
            return View();
        }
        public ActionResult KetQuaTimKiem(TimKiemSP info,int n=1)
        {
            if (info.beginGia==null)
            {
                info.beginGia = 0;
            }
            if (info.endGia==null)
            {
                info.endGia = int.MaxValue;
            }
            var List = SanPhamBUS.Search(info);
            int numrows = List.Count();
            int rowperpage = 8;
            int pageNum = n;
            int ofset = (pageNum - 1) * rowperpage;
            if (numrows % rowperpage != 0)
            {
                ViewBag.maxPage = numrows / rowperpage + 1;
            }
            else
            {
                ViewBag.maxPage = numrows / rowperpage;
            }
            string urlstr = Request.Url.AbsoluteUri;
            int pos=urlstr.IndexOf("&n=");
            urlstr = pos >= 0 ? urlstr.Remove(pos) : urlstr;
            ViewBag.url = urlstr;
            return View(List.Skip(ofset).Take(rowperpage));
        }
        public ActionResult SanPhamMoiNhap()
        {
            return View(SanPhamBUS.SanPhamMoiNhap());
        }
        public ActionResult SanPhamMuaNhieuNhat()
        {
            return View(SanPhamBUS.SanPhamMuaNhieu());
        }
    }
}

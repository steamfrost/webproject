﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WatchShopBUS.BUS;

namespace WatchShopSite.Controllers
{
    public class LoaiVoController : Controller
    {
        public ActionResult dsLoaiVo()
        {
            return View(LoaiVoBUS.GetAll());
        }
        [HttpGet]
        public ActionResult Detail(int id,int n=1)
        {
            var List = LoaiVoBUS.Detail(id,n);
            if (List.Items.Count() <= 0)
            {
                return RedirectToAction("Index", "Error");
            }
            return View(List);
        }
    }
}

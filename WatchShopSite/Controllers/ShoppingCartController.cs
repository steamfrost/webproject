﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using WatchShopSite.Models;
using WatchShopBUS.BUS;
using WatchShopConnect;

namespace WatchShopSite.Controllers
{
    public class ShoppingCartController : Controller
    {
        //
        // GET: /ShoppingCart/
        public ActionResult Index()
        {
            return View();
        }
        [Authorize(Roles="Member")]
        public ActionResult MuaHang(DONHANG dh)
        {
            var db=new WatchShopConnectDB();
            DateTime ngmua = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            int tongtien = 0;
            Guid g = Guid.NewGuid();

            List<Detail> dsHang = (List<Detail>)Session["ShoppingCart"];
            if (dsHang==null || dsHang.Count()<=0)
            {
                return DatHang("Không có sản phẩm trong giỏ hàng");
            }
            DONHANG donhang=new DONHANG{ UserID=User.Identity.GetUserId(), MADONHANG=g.ToString().Substring(0,20), TRANGTHAI="Chưa giao", TONGTIEN=0, DAXOA=false};
            donhang.TENNGUOINHAN = dh.TENNGUOINHAN;
            donhang.SDT = dh.SDT;
            donhang.DIACHI = dh.DIACHI;
            donhang.NGAYMUA = ngmua;
            db.BeginTransaction();
            if (DonHangBUS.ThemDonHang(donhang)==true)
            {
                foreach (Detail item in dsHang)
                {
                    CHITIETDONHANG ct = new CHITIETDONHANG { MADONHANG = donhang.MADONHANG, MASANPHAM = item.masanpham, SOLUONG = item.soluong, THANHTIEN = item.soluong * item.gia };
                    if (ChiTietDonHangBUS.ThemChiTietDonHang(ct))
                    {
                        SanPhamBUS.updateSoLuotMua(ct.MASANPHAM);
                        SanPhamBUS.updateSoLuongBan(ct.MASANPHAM, ct.SOLUONG);
                        SanPhamBUS.updateSoLuongTon(ct.MASANPHAM, ct.SOLUONG);
                        tongtien += item.soluong * item.gia;
                    }
                    else
                        db.AbortTransaction();
                }
            }
            else
            {
                db.AbortTransaction();
            }

            if (DonHangBUS.UpdateTongTien(donhang.MADONHANG, tongtien)==true)
            {
                db.CompleteTransaction();
                Session.Remove("ShoppingCart");
            }
            else
                db.AbortTransaction();

            return DatHang("Việc mua hàng thành công");
        }
        public ActionResult LichSuMuaHang()
        {
            return View(DonHangBUS.GetChiTietDonHangByUserID(User.Identity.GetUserId()));
        }
        public ActionResult XoaSanPham(string masanpham)
        {
            ShoppingCart.XoaSanPham(Session["ShoppingCart"], masanpham);
            return RedirectToAction("GioHang");
        }
        public ActionResult ThayDoiSoLuong(Detail item)
        {
            ShoppingCart.ThayDoiSoLuong(Session["ShoppingCart"], item);
            return RedirectToAction("GioHang");
        }
        public ActionResult GioHang()
        {
            List<Detail> lst = (List<Detail>)Session["ShoppingCart"];
            if (lst == null)
            {
                return View(new List<Detail>());
            }
            else
            {
                return View(lst);
            }
        }
        public ActionResult ThemVaoGioHang(Detail item)
        {
            if (Session["ShoppingCart"] ==null )
            {
                List<Detail> lst = new List<Detail>();
                lst.Add(item);
                Session["ShoppingCart"] = lst;
            }
            else
            {
                ShoppingCart.ThemVaoGioHang(Session["ShoppingCart"], item);
            }
            return View();
        }
        [Authorize(Roles = "Member")]
        [HttpGet]
        public ActionResult DatHang(string tt)
        {
            ViewBag.tt = tt;
            return View();
        }
        [Authorize(Roles = "Member")]
        [HttpPost]
        public ActionResult DatHang(DONHANG item)
        {
            return MuaHang(item);
        }
	}
}
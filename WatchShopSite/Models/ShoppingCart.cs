﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WatchShopSite.Models
{
    public class ShoppingCart
    {
        public static int CountItem(Object obj)
        {
            List<Detail> lst = (List<Detail>)obj;
            if (lst == null)
            {
                return 0;
            }
            else
            {
                return lst.Sum(m => m.soluong);
            }
        }
        public static void XoaTatCa(object obj)
        {
            List<Detail> lst = (List<Detail>)obj;
            foreach(var item in lst)
            {
                XoaSanPham(lst, item.masanpham);
            }
        }
        public static void ThayDoiSoLuong(object obj,Detail item)
        {
            List<Detail> lst = (List<Detail>)obj;
            int pos = lst.FindIndex(m => m.masanpham == item.masanpham);
            if (pos >= 0)
            {
                Detail old = lst.Find(m => m.masanpham == item.masanpham);
                old.soluong = item.soluong;
                lst[pos] = old;
            }
            else
            {
                lst.Add(item);
            }
        }
        public static void ThemVaoGioHang(object obj,Detail item)
        {
            List<Detail> lst = (List<Detail>)obj;
            int pos = lst.FindIndex(m => m.masanpham == item.masanpham);
            if (pos >=0)
            {
                Detail old = lst.Find(m => m.masanpham == item.masanpham);
                old.soluong += item.soluong;
                lst[pos] = old;
            }
            else
            {
                lst.Add(item);
            }
        }
        public static Int64 TinhTongTien(object obj)
        {
            Int64 number=0;
            List<Detail> lst = (List<Detail>)obj;
            if (lst!=null)
            {
                foreach (var item in lst)
                {
                    number += item.gia * item.soluong;
                }
            }
            return number;
        }
        public static void XoaSanPham(object obj, string masanpham)
        {
            List<Detail> lst = (List<Detail>)obj;
            int pos = lst.FindIndex(m => m.masanpham == masanpham);
            if (pos >= 0)
            {
                lst.RemoveAt(pos);
            }
        }
    }
}
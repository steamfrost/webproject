﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WatchShopSite.Models
{
    public class Detail
    {
        public string masanpham { get; set; }
        public string tensanpham { get; set; }
        public int gia { get; set; }
        public int soluong { get; set; }
    }
}
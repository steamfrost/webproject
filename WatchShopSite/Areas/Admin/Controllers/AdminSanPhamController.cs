﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WatchShopBUS.BUS;
using WatchShopBUS.Models;
using WatchShopConnect;

namespace WatchShopSite.Areas.Admin.Controllers
{
    public class AdminSanPhamController : Controller
    {
        private string FileUpload(HttpPostedFileBase file)
        {
            if (file != null)
            {
                string pic = System.IO.Path.GetFileName(file.FileName);
                string path = System.IO.Path.Combine(
                                       Server.MapPath("~/Content/img/item"), pic);
                // file is uploaded
                file.SaveAs(path);

                // save the image path path to the database or you can send image 
                // directly to database
                // in-case if you want to store byte[] ie. for DB
                using (MemoryStream ms = new MemoryStream())
                {
                    file.InputStream.CopyTo(ms);
                    byte[] array = ms.GetBuffer();
                }

            }
            // after successfully uploading redirect the user
            return file.FileName;
        }
        public ActionResult ChiTietSanPham(string MASANPHAM)
        {
            var data = SanPhamBUS.GetItemByID(MASANPHAM);
            return View(data);
        }
        public ActionResult ThemAnhSanPham(int page, string MASANPHAM)
        {
            ViewBag.page = page;
            ViewBag.ListImage = HinhAnhBUS.GetImageBySp(MASANPHAM);
            return View(new WatchShopConnect.HINHANH() { MASANPHAM=MASANPHAM });
        }
        [HttpPost]
        public ActionResult DoiAnhSanPham(HINHANH item)
        {
            HinhAnhBUS.DoiAnhSanPham(item);
            return RedirectToAction("index", "AdminHome");
        }
        [HttpPost]
        public ActionResult ThemAnhSanPham(HINHANH item)
        {
            item.TENHINHANH = FileUpload(Request.Files[0]);
            HinhAnhBUS.AddNewImg(item);
            return RedirectToAction("index", "AdminHome");
        }
        /// <summary>
        /// Xóa sản phẩm
        /// </summary>
        /// <param name="MASANPHAM"></param>
        /// <returns></returns>
        public ActionResult XoaSanPham(int page,string MASANPHAM)
        {
            SanPhamBUS.CapNhatTrangThai(MASANPHAM,true);
            return RedirectToAction("DanhSachSanPham", new { dsSanPhamPage=page});
        }
        /// <summary>
        /// Phục hồi sản phẩm
        /// </summary>
        /// <param name="MASANPHAM"></param>
        /// <returns></returns>
        public ActionResult PhucHoiSanPham(int page, string MASANPHAM)
        {
            SanPhamBUS.CapNhatTrangThai(MASANPHAM,false);
            return RedirectToAction("DanhSachSanPham", new { dsSanPhamPage = page });
        }
        /// <summary>
        /// Xuất danh sách sản phẩm
        /// </summary>
        /// <param name="str"></param>
        /// <param name="dsSanPhamPage"></param>
        /// <returns></returns>
        public ActionResult DanhSachSanPham(string str,int dsSanPhamPage=1)
        {
            var data = SanPhamBUS.GetAllAdminView(str,dsSanPhamPage);
            ViewBag.page = dsSanPhamPage;
            ViewBag.str = str;
            return View(data);
        }
        [ValidateInput(false)]
        public ActionResult CapNhatSanPham(int page,WatchShopConnect.SANPHAM item)
        {
            ViewBag.page=page;
            if (item.TENSANPHAM != null)
            {
                if (item.GIABAN < item.GIAGOC)
                    item.COGIAMGIA = true;
                else
                    item.COGIAMGIA = false;
                SanPhamBUS.CapNhatSanPham(item);
                return RedirectToAction("DanhSachSanPham", new {dsSanPhamPage=page});
            }
            else
            {
                return View(SanPhamBUS.GetItemByID_Admin(item.MASANPHAM));
            }
        }
        public ActionResult ThemSanPham(int page)
        {
            ViewBag.NSX = NhaSanXuatBUS.GetAll();
            ViewBag.NangLuong = LoaiNangLuongBUS.GetAll();
            ViewBag.LoaiVo = LoaiVoBUS.GetAll();
            ViewBag.LoaiDay = LoaiDayBUS.GetAll();
            ViewBag.page = page;
            return View();
        }
        /// <summary>
        /// Thêm sản phẩm
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ThemSanPham(WatchShopConnect.SANPHAM item)
        {
            if (ModelState.IsValid)
            {
                if (item.GIABAN < item.GIAGOC)
                    item.COGIAMGIA = true;
                else
                    item.COGIAMGIA = false;
                item.IMAGE = FileUpload(Request.Files[0]);
                SanPhamBUS.AddNewItem(item);
            }
            return RedirectToAction("index", "AdminHome");
        }
        public ActionResult ThongKe()
        {
            return View(SanPhamBUS.SanPhamMuaNhieuTop10());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WatchShopBUS.BUS;
using WatchShopConnect;

namespace WatchShopSite.Areas.Admin.Controllers
{
    public class AdminNhaSanXuatController : Controller
    {
        //
        // GET: /Admin/AdminNhaSanXuat/

        public ActionResult DanhSachNhaSanXuat()
        {
            var data = NhaSanXuatBUS.GetAll();
            return View(data);
        }
        public ActionResult ThemNhaSanXuat(NHASANXUAT item)
        {
            if (item.MANHASANXUAT != null)
            {
                NhaSanXuatBUS.AddNewItem(item);
                return RedirectToAction("DanhSachNhaSanXuat");
            }
            else
            {
                return View();
            }
        }
        public ActionResult XoaNhaSanXuat(string MANSX)
        {
            NhaSanXuatBUS.DeleteItemByID(MANSX);
            return RedirectToAction("DanhSachNhaSanXuat");
        }
        public ActionResult PhucHoiNhaSanXuat(string MANSX)
        {
            NhaSanXuatBUS.PhucHoiNhaSanXuat(MANSX);
            return RedirectToAction("DanhSachNhaSanXuat");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WatchShopBUS.BUS;

namespace WatchShopSite.Areas.Admin.Controllers
{
    [Authorize(Roles="Admin")]
    public class AdminHomeController : Controller
    {
        //
        // GET: /Admin/AdminHome/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult dsSanPham()
        {
            return View();
        }
        public ActionResult dsNhaSanXuat()
        {
            return View();
        }
    }
}

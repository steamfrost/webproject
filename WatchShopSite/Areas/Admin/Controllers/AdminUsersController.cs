﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using ASP.NET_MVC5_Bootstrap3_3_1_LESS.Models;
using ASP.NET_MVC5_Bootstrap3_3_1_LESS.Controllers;
using WatchShopBUS.BUS;
using WatchShopConnect;
using WatchShopSite.Models;

namespace WatchShopSite.Areas.Admin.Controllers
{
    public class AdminUsersController : AccountController
    {
        //
        // GET: /Admin/AdminUsers/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult DanhSachUser(int page=1)
        {
            var lst = NguoiDungBUS.GetAll(page);
            ViewBag.Current = User.Identity.GetUserId();
            return View(lst);
        }
        public ActionResult XoaTaiKhoan(NGUOIDUNG item)
        {
            item.DAXOA = true;
            NguoiDungBUS.XoaTaiKhoan(item);
            return View();
        }
        public ActionResult PhucHoiTaiKhoan(NGUOIDUNG item)
        {
            item.DAXOA = false;
            NguoiDungBUS.XoaTaiKhoan(item);
            return View();
        }

        public ActionResult PhanQuyen(string user,string per)
        {
            if (user != "")
            {
                string reper = "";
                if (per == "Admin")
                {
                    reper = "Member";
                }
                else
                {
                    if (per == "Member")
                    {
                        reper = "Admin";
                    }
                }
                UserManager.RemoveFromRole(UserManager.FindByName(user).Id, reper);
                UserManager.AddToRole(UserManager.FindByName(user).Id, per);
                ViewBag.pq = per;
            }
            return View();
        }
        public ActionResult frmCapNhat(NGUOIDUNG model)
        {
            NGUOIDUNG item = NguoiDungBUS.GetUser(model.UserID);
            IdentityUser user = UserManager.FindById(model.UserID);
            RegistorInf result = new RegistorInf { UserName=user.UserName, Address = item.DIACHI, BirthDate = item.NGAYSINH, Email = item.EMAIL, FullName = item.HOTEN, IdentityCart = item.CMND, PhoneNumber = item.SDT };
            return View(result);
        }
        public ActionResult CapNhatTaiKhoan(RegistorInf item)
        {
            if (item == null)
            {
                return RedirectToAction("DanhSachUser", new { page=1});
            }
            else
            {
                    var user = new WatchShopConnectDB().FirstOrDefault<AspNetUser>("select Id from AspNetUsers where UserName = '" + item.UserName + "'");
                    NGUOIDUNG nd = new NGUOIDUNG { UserID = user.Id, SDT = item.PhoneNumber, CMND = item.IdentityCart, HOTEN = item.FullName, EMAIL = item.Email, NGAYSINH = item.BirthDate, DIACHI = item.Address };    
                    NguoiDungBUS.CapNhatThongTin(nd);
                    return RedirectToAction("DanhSachUser", new { page=1});
            }
        }
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WatchShopBUS.BUS;

namespace WatchShopSite.Areas.Admin.Controllers
{
    public class AdminDonHangController : Controller
    {
        //
        // GET: /Admin/AdminDonHang/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ChiTietDonHang(string id)
        {
            var lst = ChiTietDonHangBUS.GetDetail(id);
            ViewBag.dh = DonHangBUS.GetItemByID(id);
            return View(lst);
        }
        public ActionResult DanhSachDonHang(int page=1)
        {
            var lst = DonHangBUS.DanhSachDonHang(page);
            return View(lst);
        }
        public ActionResult CapNhatTrangThai(string id,string tt)
        {
            DonHangBUS.CapNhatTrangThai(id, tt);
            ViewBag.tt = tt;
            return View();
        }
	}
}
function ChangeValue(a) {
    var i = document.getElementById('soluong').value;
    if (a.id === 'Tru') {
        i -= 1;
        if (i < 1)
            i = 1;
        document.getElementById('soluong').value = Number(i);
    }
    else {
        if (a.id === 'Cong') {
            i = Number(i) + 1;
            document.getElementById('soluong').value = Number(i);
        }
    }
}
var itema = document.getElementsByClassName('panel panel-success');
var itemb = document.getElementsByClassName('ItemInfo');
var n = itema.length;
for (i = 0; i < n; i++) {
    itemb[i].style.height = itema[i].style.height;
    itemb[i].style.width = itema[i].style.width;
}
    $('.btn-warning').on('click', function () {
        var cart = $('#CartIcon');
        var imgtodrag = $(this).parent('.ItemInfo').parent('.panel-body').find("img").eq(0);
        if (imgtodrag) {
            var imgclone = imgtodrag.clone()
                .offset({
                    top: imgtodrag.offset().top,
                    left: imgtodrag.offset().left
                })
                .css({
                    'opacity': '0.5',
                    'position': 'absolute',
                    'height': '150px',
                    'width': '150px',
                    'z-index': '100'
                })
                .appendTo($('body'))
                .animate({
                    'top': cart.offset().top + 10,
                    'left': cart.offset().left + 10,
                    'width': 75,
                    'height': 75
                }, 1000, 'easeInOutExpo');
            setTimeout(function () {
                cart.effect("pulsate", {
                    times: 2
                }, 50);
            }, 1500);
            imgclone.animate({
                'width': 0,
                'height': 0
            }, function () {
                $(this).detach()
            });
        }
    });

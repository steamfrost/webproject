﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WatchShopConnect;

namespace WatchShopBUS.BUS
{
    public class SanPhamBUS
    {
        /// <summary>
        /// Lấy tất cả thông tin sản phẩm cho admin xem
        /// </summary>
        /// <returns></returns>
        public static PetaPoco.Page<SP_VIEW> GetAllAdminView(string str,int n)
        {
            var data =new WatchShopConnectDB().Page<SP_VIEW>(n,8,"select MASANPHAM,TENSANPHAM,DAXOA from SP_VIEW where TENSANPHAM like N'%"+str+"%'");
            return data;
        }
        public static bool updateSoLuongBan(string masp, int slb)
        {
            var db = new WatchShopConnectDB();
            try
            {
                SANPHAM sp = db.FirstOrDefault<SANPHAM>("select MASANPHAM,SOLUONGBAN from SANPHAM where MASANPHAM = '" + masp + "'");
                sp.SOLUONGBAN += slb;
                db.Update("SANPHAM", "MASANPHAM", sp, new List<string> { "SOLUONGBAN"});
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static bool updateSoLuotMua(string masp)
        {
            var db = new WatchShopConnectDB();
            try
            {
                SANPHAM sp = db.FirstOrDefault<SANPHAM>("select MASANPHAM,LUOTMUA from SANPHAM where MASANPHAM = '" + masp + "'");
                sp.LUOTMUA += 1;
                db.Update("SANPHAM", "MASANPHAM", sp, new List<string> { "LUOTMUA" });
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static bool updateSoLuongTon(string masp,int sl)
        {
            var db = new WatchShopConnectDB();
            try
            {
                SANPHAM sp = db.FirstOrDefault<SANPHAM>("select MASANPHAM,SOLUONGTON from SANPHAM where MASANPHAM = '" + masp + "'");
                if (sp.SOLUONGTON>sl)
                {
                    sp.SOLUONGTON -= sl;
                }
                else
                {
                    db.AbortTransaction();
                }
                db.Update("SANPHAM", "MASANPHAM", sp, new List<string> { "SOLUONGTON" });
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static void CapNhatSanPham(SANPHAM item)
        {
            SANPHAM oldsp = SanPhamBUS.GetItemByID_Admin(item.MASANPHAM);
            oldsp.XUATXU = item.XUATXU;
            oldsp.GIAGOC = item.GIAGOC;
            oldsp.GIABAN = item.GIABAN;
            oldsp.SOLUONGTON = item.SOLUONGTON;
            oldsp.MOTA = item.MOTA;
            oldsp.HUONGDAN = item.HUONGDAN;
            new WatchShopConnectDB().Update("SANPHAM", "MASANPHAM",oldsp);
        }
        /// <summary>
        /// Lấy ra 8 sản phẩm mới đăng lên trang chủ
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<SP_VIEW> SanPhamMoiNhap()
        {
            return new WatchShopConnectDB().Query<SP_VIEW>("select TOP 8 * from SP_VIEW where DAXOA=0 ORDER BY NGAYDANG desc");
        }
        /// <summary>
        /// Lấy ra 8 sản phẩm có lượt mua nhiều nhất lên trang chủ
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<SP_VIEW> SanPhamMuaNhieu()
        {
            return new WatchShopConnectDB().Query<SP_VIEW>("select TOP 8 * from SP_VIEW where DAXOA=0 ORDER BY LUOTMUA desc");
        }
        /// <summary>
        /// Tìm sản phẩm cho admin
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static PetaPoco.Page<SP_VIEW> SearchSanPhamAdminView(string str,int n)
        {
            var data = new WatchShopConnectDB().Page<SP_VIEW>(n, 8, "select MASANPHAM,TENSANPHAM,DAXOA from SP_VIEW where DAXOA=0 and TENSANPHAM like N'%"+str+"%'");
            return data;
        }
        /// <summary>
        /// lấy phần tử sản phẩm cho slider
        /// </summary>
        /// <param name="numItem"></param>
        /// <returns></returns>
        public static IEnumerable<SANPHAM> GetItemSale(int numItem)
        {
            return new WatchShopConnectDB().Query<SANPHAM>("select top " + numItem + " MASANPHAM, TENSANPHAM,GIAGOC,GIABAN,IMAGE from SANPHAM where COGIAMGIA='True' and DAXOA=0");
        }
        /// <summary>
        /// lấy tất cả sản phẩm có chương trình giảm giá
        /// </summary>
        /// <returns></returns>
        public static PetaPoco.Page<SP_VIEW> GetAllItemSale(int page)
        {
            return new WatchShopConnectDB().Page<SP_VIEW>(page,8,"select * from SP_VIEW where GIABAN<GIAGOC and DAXOA=0");
        }
        public static SANPHAM GetItemByID_Admin(string id)
        {
            return new WatchShopConnectDB().SingleOrDefault<SANPHAM>("select * from SANPHAM where MASANPHAM='" + id + "'");
        }
        /// <summary>
        /// Lấy thông tin chi tiết của 1 sản phẩm
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static SP_VIEW GetItemByID(string id)
        {
            return new WatchShopConnectDB().SingleOrDefault<SP_VIEW>("select * from SP_VIEW where DAXOA=0 and MASANPHAM='"+id+"'");
        }
        public static PetaPoco.Page<SP_VIEW> getItemByNSXID(string nsxid)
        {
            return new WatchShopConnectDB().Page<SP_VIEW>(1, 6, "select * from SP_VIEW where DAXOA=0 and NHASANXUAT = '" + nsxid + "'");
        }
        /// <summary>
        /// Thêm sản phẩm mới
        /// </summary>
        /// <param name="item"></param>
        public static void AddNewItem(SANPHAM item)
        {
            WatchShopConnectDB connect = new WatchShopConnectDB();
            connect.Insert("SANPHAM", "MASANPHAM", item);
            connect.Insert("HINHANH", new HINHANH { MASANPHAM = item.MASANPHAM, TENHINHANH = item.IMAGE });
        }
        /// <summary>
        /// Chức năng xóa sản phẩm dành cho admin
        /// </summary>
        /// <param name="id"></param>
        public static void CapNhatTrangThai(string id, bool trangthai)
        {
            if (trangthai == true)
            {
                new WatchShopConnectDB().Execute("update SANPHAM set DAXOA=1 where MASANPHAM='" + id + "'");
            }
            else
            {
                new WatchShopConnectDB().Execute("update SANPHAM set DAXOA=0 where MASANPHAM='" + id + "'");
            }
        }
        /// <summary>
        /// Cập nhật thông tin của sản phẩm
        /// </summary>
        /// <param name="sp"></param>
        public static void UpdateItemByID(SANPHAM sp)
        {

        }
        /// <summary>
        /// Tìm kiếm sản phẩm theo nhiều tiêu chí
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public static IEnumerable<SP_VIEW> Search(Models.TimKiemSP info)
        {
            var db=new WatchShopConnectDB();
            var sql = PetaPoco.Sql.Builder.Append(";EXEC SEARCH_SANPHAM @TENSANPHAM,@GIATU,@GIADEN,@NSX,@LOAIDAY,@LOAIVO,@LOAINANGLUONG", new { TENSANPHAM=info.TenSanPham,GIATU=info.beginGia,GIADEN=info.endGia,NSX=info.NhaSanXuat,LOAIVO=info.LoaiVo,LOAIDAY=info.LoaiDay,LOAINANGLUONG=info.LoaiNangLuong});
            IEnumerable<SP_VIEW> data = db.Query<SP_VIEW>(sql);
            return data;
        }
        public static PetaPoco.Page<SP_VIEW> TimSanPhamTheoGia(int min,int max,int n)
        {
            return new WatchShopConnectDB().Page<SP_VIEW>(n, 8, string.Format("select * from SP_VIEW where DAXOA=0 and GIABAN>= {0} and GIABAN <= {1}", min, max));
        }
        public static IEnumerable<SANPHAM> SanPhamMuaNhieuTop10()
        {
            return new WatchShopConnectDB().Query<SANPHAM>("select TOP 10 TENSANPHAM,SOLUONGBAN from SANPHAM where DAXOA=0 ORDER BY SOLUONGBAN desc");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WatchShopConnect;

namespace WatchShopBUS.BUS
{
    public class NguoiDungBUS
    {
        public static void ThemNguoiDung(NGUOIDUNG item)
        {
            new WatchShopConnectDB().Insert("NGUOIDUNG", item);
        }
        public static void CapNhatThongTin(NGUOIDUNG item)
        {
            new WatchShopConnectDB().Update("NGUOIDUNG", "UserID", item);
        }
        public static NGUOIDUNG GetUser(string userID)
        {
            return new WatchShopConnectDB().SingleOrDefault<NGUOIDUNG>("select * from NGUOIDUNG where UserID ='" + userID + "'");
        }
        public static PetaPoco.Page<ViewUser> GetAll(int page)
        {
            return new WatchShopConnectDB().Page<ViewUser>(page,8,"select * from ViewUser");
        }
        public static void XoaTaiKhoan(NGUOIDUNG item)
        {
            new WatchShopConnectDB().Update("NGUOIDUNG", "UserID", item, new List<string>() { "DAXOA"});
        }
        public static void PhucHoiTaiKhoan(NGUOIDUNG item)
        {
            new WatchShopConnectDB().Update("NGUOIDUNG", "UserID", item, new List<string>() { "DAXOA" });
        }
    }
}

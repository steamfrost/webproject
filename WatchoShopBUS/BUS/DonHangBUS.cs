﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WatchShopBUS.Models;
using WatchShopConnect;

namespace WatchShopBUS.BUS
{
    public class DonHangBUS
    {
        public static bool ThemDonHang(DONHANG donhang)
        {
            try
            {
                new WatchShopConnectDB().Insert("DONHANG", "MADONHANG", donhang);
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }
        public static bool UpdateTongTien(string dhid,int tongtien)
        {
            try
            {
                var db = new WatchShopConnectDB();
                DONHANG dh = db.FirstOrDefault<DONHANG>("select * from DONHANG where MADONHANG = '" + dhid + "'");
                dh.TONGTIEN = tongtien;
                db.Update("DONHANG", "MADONHANG", dh);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static IEnumerable<ViewDonHang> GetChiTietDonHangByUserID(string userID)
        {
            return new WatchShopConnectDB().Query<ViewDonHang>("select dh.MADONHANG, dh.NGAYMUA, dh.TONGTIEN, dh.TRANGTHAI, sp.MASANPHAM, sp.TENSANPHAM, ct.SOLUONG,ct.THANHTIEN from CHITIETDONHANG ct,DONHANG dh, SANPHAM sp where ct.MASANPHAM = sp.MASANPHAM and dh.UserID = '"+userID+"' and ct.MADONHANG = dh.MADONHANG and dh.DAXOA=0");
        }

        public static PetaPoco.Page<dsDonHangView> DanhSachDonHang(int page)
        {
            string sql = "select dh.MADONHANG,nd.HOTEN,dh.NGAYMUA,dh.TONGTIEN,dh.TRANGTHAI from DONHANG dh,NGUOIDUNG nd where dh.UserID=nd.UserID order by dh.NGAYMUA desc";
            var lst = new WatchShopConnectDB().Page<dsDonHangView>(page,8,sql);
            return lst;
        }
        public static void CapNhatTrangThai(string id,string tt)
        {
            new WatchShopConnectDB().Update("DONHANG", "MADONHANG", new DONHANG { MADONHANG = id, TRANGTHAI = tt }, new List<string> { "TRANGTHAI" });
        }
        public static DONHANG GetItemByID(string id)
        {
            return new WatchShopConnectDB().FirstOrDefault<DONHANG>("select * from DONHANG where MADONHANG = '"+id+"'");
        }
    }
}

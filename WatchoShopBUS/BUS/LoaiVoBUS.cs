﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WatchShopConnect;

namespace WatchShopBUS.BUS
{
    public class LoaiVoBUS
    {
        public static IEnumerable<LOAIVO> GetAll()
        {
            return new WatchShopConnectDB().Query<LOAIVO>("select * from LOAIVO");
        }

        public static PetaPoco.Page<SP_VIEW> Detail(int id,int page)
        {
            string sql = "select sp.* from SP_VIEW sp,LOAIVO lv where sp.LOAIVO=lv.TENLOAI and sp.DAXOA=0 and lv.ID=" + id;
            return new WatchShopConnectDB().Page<SP_VIEW>(page,8,sql);
        }
    }
}

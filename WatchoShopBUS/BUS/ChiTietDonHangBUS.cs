﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WatchShopBUS.Models;
using WatchShopConnect;

namespace WatchShopBUS.BUS
{
    public class ChiTietDonHangBUS
    {
        public static bool ThemChiTietDonHang(CHITIETDONHANG item)
        {
            try
            {
                new WatchShopConnectDB().Insert("CHITIETDONHANG",item);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static IEnumerable<DonHangDetail> GetDetail(string id)
        {
            return new WatchShopConnectDB().Query<DonHangDetail>("select sp.MASANPHAM,sp.TENSANPHAM,ct.SOLUONG,ct.THANHTIEN from SANPHAM sp, CHITIETDONHANG ct where sp.MASANPHAM=ct.MASANPHAM and ct.MADONHANG = '"+id+"'");
        }
    }
}

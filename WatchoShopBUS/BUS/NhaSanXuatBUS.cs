﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WatchShopConnect;

namespace WatchShopBUS.BUS
{
    public class NhaSanXuatBUS
    {
        public static IEnumerable<NHASANXUAT> GetAll()
        {
            return new WatchShopConnectDB().Query<NHASANXUAT>("select * from NHASANXUAT");
        }
        public static IEnumerable<NHASANXUAT> GetAllUserView()
        {
            return new WatchShopConnectDB().Query<NHASANXUAT>("select * from NHASANXUAT where DAXOA=0");
        }
        public static void AddNewItem(NHASANXUAT item)
        {
            WatchShopConnectDB connect = new WatchShopConnectDB();
            connect.Insert("NHASANXUAT", "MANHASANXUAT", item);
        }
        public static NHASANXUAT GetItemByID(string id)
        {
            return new WatchShopConnectDB().SingleOrDefault<NHASANXUAT>("select * from NHASANXUAT where MANHASANXUAT='" + id + "'");
        }
        public static void UpdateItem(NHASANXUAT item)
        {
            WatchShopConnectDB connect = new WatchShopConnectDB();
            connect.Update("NHASANXUAT", "MANHASANXUAT", item);
        }
        public static void DeleteItemByID(string id)
        {
            new WatchShopConnectDB().Execute(string.Format(";EXEC trangthai_nhasanxuat '{0}',1",id));
        }
        public static void PhucHoiNhaSanXuat(string id)
        {
            new WatchShopConnectDB().Execute(string.Format(";EXEC trangthai_nhasanxuat '{0}',0", id));
        }
        /// <summary>
        /// Lấy thông tin sản phẩm của 1 nhà sản xuất
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static PetaPoco.Page<SP_VIEW> GetDetail(string id,int page)
        {
            string sql="select p.* from SP_VIEW p, NHASANXUAT nsx where p.DAXOA=0 and p.TENNHASANXUAT=nsx.TENNHASANXUAT and nsx.MANHASANXUAT='"+id+"'";
            return new WatchShopConnectDB().Page<SP_VIEW>(page,8,sql);
        }
    }
}

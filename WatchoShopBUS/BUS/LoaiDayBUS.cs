﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WatchShopConnect;

namespace WatchShopBUS.BUS
{
    public class LoaiDayBUS
    {
        public static IEnumerable<LOAIDAY> GetAll()
        {
            return new WatchShopConnectDB().Query<LOAIDAY>("select * from LOAIDAY");
        }
        public static PetaPoco.Page<SP_VIEW> Detail(int id,int page)
        {
            string sql = "select sp.* from SP_VIEW sp,LOAIDAY lv where sp.LOAIDAY=lv.TENLOAI and sp.DAXOA=0 and lv.ID=" + id;
            return new WatchShopConnectDB().Page<SP_VIEW>(page,8,sql);
        }
    }
}

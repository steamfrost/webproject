﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WatchShopBUS.Models
{
    public class ViewDonHang
    {
        public string MADONHANG { get; set; }
        public DateTime NGAYMUA { get; set; }
        public int TONGTIEN { get; set; }
        public string TRANGTHAI { get; set; }
        public string MASANPHAM { get; set; }
        public string TENSANPHAM { get; set; }
        public int SOLUONG { get; set; }
        public int THANHTIEN { get; set; }
    }
}

﻿
// This file was automatically generated by the PetaPoco T4 Template
// Do not make changes directly to this file - edit the template instead
// 
// The following connection settings were used to generate this file
// 
//     Connection String Name: `WatchShopConnect`
//     Provider:               `System.Data.SqlClient`
//     Connection String:      `Data Source=ANH-PC;Initial Catalog=WebDatabase;Integrated Security=True`
//     Schema:                 ``
//     Include Views:          `True`

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;
using System.ComponentModel.DataAnnotations;

namespace WatchShopConnect
{
	public partial class WatchShopConnectDB : Database
	{
		public WatchShopConnectDB() 
			: base("WatchShopConnect")
		{
			CommonConstruct();
		}

		public WatchShopConnectDB(string connectionStringName) 
			: base(connectionStringName)
		{
			CommonConstruct();
		}
		
		partial void CommonConstruct();
		
		public interface IFactory
		{
			WatchShopConnectDB GetInstance();
		}
		
		public static IFactory Factory { get; set; }
        public static WatchShopConnectDB GetInstance()
        {
			if (_instance!=null)
				return _instance;
				
			if (Factory!=null)
				return Factory.GetInstance();
			else
				return new WatchShopConnectDB();
        }

		[ThreadStatic] static WatchShopConnectDB _instance;
		
		public override void OnBeginTransaction()
		{
			if (_instance==null)
				_instance=this;
		}
		
		public override void OnEndTransaction()
		{
			if (_instance==this)
				_instance=null;
		}
        
		public class Record<T> where T:new()
		{
			public static WatchShopConnectDB repo { get { return WatchShopConnectDB.GetInstance(); } }
			public bool IsNew() { return repo.IsNew(this); }
			public object Insert() { return repo.Insert(this); }
			public void Save() { repo.Save(this); }
			public int Update() { return repo.Update(this); }
			public int Update(IEnumerable<string> columns) { return repo.Update(this, columns); }
			public static int Update(string sql, params object[] args) { return repo.Update<T>(sql, args); }
			public static int Update(Sql sql) { return repo.Update<T>(sql); }
			public int Delete() { return repo.Delete(this); }
			public static int Delete(string sql, params object[] args) { return repo.Delete<T>(sql, args); }
			public static int Delete(Sql sql) { return repo.Delete<T>(sql); }
			public static int Delete(object primaryKey) { return repo.Delete<T>(primaryKey); }
			public static bool Exists(object primaryKey) { return repo.Exists<T>(primaryKey); }
			public static bool Exists(string sql, params object[] args) { return repo.Exists<T>(sql, args); }
			public static T SingleOrDefault(object primaryKey) { return repo.SingleOrDefault<T>(primaryKey); }
			public static T SingleOrDefault(string sql, params object[] args) { return repo.SingleOrDefault<T>(sql, args); }
			public static T SingleOrDefault(Sql sql) { return repo.SingleOrDefault<T>(sql); }
			public static T FirstOrDefault(string sql, params object[] args) { return repo.FirstOrDefault<T>(sql, args); }
			public static T FirstOrDefault(Sql sql) { return repo.FirstOrDefault<T>(sql); }
			public static T Single(object primaryKey) { return repo.Single<T>(primaryKey); }
			public static T Single(string sql, params object[] args) { return repo.Single<T>(sql, args); }
			public static T Single(Sql sql) { return repo.Single<T>(sql); }
			public static T First(string sql, params object[] args) { return repo.First<T>(sql, args); }
			public static T First(Sql sql) { return repo.First<T>(sql); }
			public static List<T> Fetch(string sql, params object[] args) { return repo.Fetch<T>(sql, args); }
			public static List<T> Fetch(Sql sql) { return repo.Fetch<T>(sql); }
			public static List<T> Fetch(long page, long itemsPerPage, string sql, params object[] args) { return repo.Fetch<T>(page, itemsPerPage, sql, args); }
			public static List<T> Fetch(long page, long itemsPerPage, Sql sql) { return repo.Fetch<T>(page, itemsPerPage, sql); }
			public static List<T> SkipTake(long skip, long take, string sql, params object[] args) { return repo.SkipTake<T>(skip, take, sql, args); }
			public static List<T> SkipTake(long skip, long take, Sql sql) { return repo.SkipTake<T>(skip, take, sql); }
			public static Page<T> Page(long page, long itemsPerPage, string sql, params object[] args) { return repo.Page<T>(page, itemsPerPage, sql, args); }
			public static Page<T> Page(long page, long itemsPerPage, Sql sql) { return repo.Page<T>(page, itemsPerPage, sql); }
			public static IEnumerable<T> Query(string sql, params object[] args) { return repo.Query<T>(sql, args); }
			public static IEnumerable<T> Query(Sql sql) { return repo.Query<T>(sql); }
		}
	}
	

    
	[TableName("dbo.LOAINANGLUONG")]
	[PrimaryKey("ID")]
	[ExplicitColumns]
    public partial class LOAINANGLUONG : WatchShopConnectDB.Record<LOAINANGLUONG>  
    {
		[Column] public int ID { get; set; }
		[Column] public string TENLOAI { get; set; }
		[Column] public bool DAXOA { get; set; }
	}
    
	[TableName("dbo.LOAIDAY")]
	[PrimaryKey("ID")]
	[ExplicitColumns]
    public partial class LOAIDAY : WatchShopConnectDB.Record<LOAIDAY>  
    {
		[Column] public int ID { get; set; }
		[Column] public string TENLOAI { get; set; }
		[Column] public bool DAXOA { get; set; }
	}
    
	[TableName("dbo.AspNetRoles")]
	[PrimaryKey("Id", AutoIncrement=false)]
	[ExplicitColumns]
    public partial class AspNetRole : WatchShopConnectDB.Record<AspNetRole>  
    {
		[Column] public string Id { get; set; }
		[Column] public string Name { get; set; }
	}
    
	[TableName("dbo.__MigrationHistory")]
	[PrimaryKey("MigrationId", AutoIncrement=false)]
	[ExplicitColumns]
    public partial class __MigrationHistory : WatchShopConnectDB.Record<__MigrationHistory>  
    {
		[Column] public string MigrationId { get; set; }
		[Column] public string ContextKey { get; set; }
		[Column] public byte[] Model { get; set; }
		[Column] public string ProductVersion { get; set; }
	}
    
	[TableName("dbo.AspNetUsers")]
	[PrimaryKey("Id", AutoIncrement=false)]
	[ExplicitColumns]
    public partial class AspNetUser : WatchShopConnectDB.Record<AspNetUser>  
    {
		[Column] public string Id { get; set; }
		[Column] public string Email { get; set; }
		[Column] public bool EmailConfirmed { get; set; }
		[Column] public string PasswordHash { get; set; }
		[Column] public string SecurityStamp { get; set; }
		[Column] public string PhoneNumber { get; set; }
		[Column] public bool PhoneNumberConfirmed { get; set; }
		[Column] public bool TwoFactorEnabled { get; set; }
		[Column] public DateTime? LockoutEndDateUtc { get; set; }
		[Column] public bool LockoutEnabled { get; set; }
		[Column] public int AccessFailedCount { get; set; }
		[Column] public string UserName { get; set; }
	}
    
	[TableName("dbo.NHASANXUAT")]
	[PrimaryKey("MANHASANXUAT", AutoIncrement=false)]
	[ExplicitColumns]
    public partial class NHASANXUAT : WatchShopConnectDB.Record<NHASANXUAT>  
    {
		[Column] public string MANHASANXUAT { get; set; }
		[Column] public string TENNHASANXUAT { get; set; }
		[Column] public bool DAXOA { get; set; }
	}
    
	[TableName("dbo.NGUOIDUNG")]
	[PrimaryKey("UserID", AutoIncrement=false)]
	[ExplicitColumns]
    public partial class NGUOIDUNG : WatchShopConnectDB.Record<NGUOIDUNG>  
    {
        [Required]
		[Column] public string UserID { get; set; }
        [Required]
		[Column] public string HOTEN { get; set; }
        [Required]
		[Column] public DateTime NGAYSINH { get; set; }
        [Required]
		[Column] public string EMAIL { get; set; }
        [Required]
        [StringLength(12, MinimumLength = 7, ErrorMessage = "Số điện thoại phải từ 7 tới 12 số")]
		[Column] public string SDT { get; set; }
        [Required]
        [StringLength(13, MinimumLength = 9)]
		[Column] public string CMND { get; set; }
        [Required]
		[Column] public string DIACHI { get; set; }
		[Column] public bool DAXOA { get; set; }
	}
    
	[TableName("dbo.AspNetUserRoles")]
	[PrimaryKey("UserId", AutoIncrement=false)]
	[ExplicitColumns]
    public partial class AspNetUserRole : WatchShopConnectDB.Record<AspNetUserRole>  
    {
		[Column] public string UserId { get; set; }
		[Column] public string RoleId { get; set; }
	}
    
	[TableName("dbo.AspNetUserLogins")]
	[PrimaryKey("LoginProvider", AutoIncrement=false)]
	[ExplicitColumns]
    public partial class AspNetUserLogin : WatchShopConnectDB.Record<AspNetUserLogin>  
    {
		[Column] public string LoginProvider { get; set; }
		[Column] public string ProviderKey { get; set; }
		[Column] public string UserId { get; set; }
	}
    
	[TableName("dbo.AspNetUserClaims")]
	[PrimaryKey("Id")]
	[ExplicitColumns]
    public partial class AspNetUserClaim : WatchShopConnectDB.Record<AspNetUserClaim>  
    {
		[Column] public int Id { get; set; }
		[Column] public string UserId { get; set; }
		[Column] public string ClaimType { get; set; }
		[Column] public string ClaimValue { get; set; }
	}
    
	[TableName("dbo.DONHANG")]
	[PrimaryKey("MADONHANG", AutoIncrement=false)]
	[ExplicitColumns]
    public partial class DONHANG : WatchShopConnectDB.Record<DONHANG>  
    {
		[Column] public string MADONHANG { get; set; }
		[Column] public string UserID { get; set; }
		[Column] public DateTime NGAYMUA { get; set; }
		[Column] public int TONGTIEN { get; set; }
		[Column] public string TRANGTHAI { get; set; }
		[Column] public bool DAXOA { get; set; }
        [Required]
        [Display(Name="Tên người nhận")]
		[Column] public string TENNGUOINHAN { get; set; }
        [Required]
        [Display(Name="Địa chỉ")]
		[Column] public string DIACHI { get; set; }
        [Required]
        [Display(Name="Số điện thoại")]
        [StringLength(12,MinimumLength=7,ErrorMessage="Số điện thoại phải từ 7 tới 12 số")]
		[Column] public string SDT { get; set; }
	}
    
	[TableName("dbo.SANPHAM")]
	[PrimaryKey("MASANPHAM", AutoIncrement=false)]
	[ExplicitColumns]
    public partial class SANPHAM : WatchShopConnectDB.Record<SANPHAM>  
    {
        [Required(ErrorMessage="Bắt buộc")]
		[Column] public string MASANPHAM { get; set; }
        [Required(ErrorMessage = "Bắt buộc")]
		[Column] public string TENSANPHAM { get; set; }
        [Required(ErrorMessage = "Bắt buộc")]
		[Column] public string XUATXU { get; set; }
        [Required(ErrorMessage = "Bắt buộc")]
        [Range(1000,int.MaxValue,ErrorMessage="Lỗi nhập liệu")]

		[Column] public int GIAGOC { get; set; }
        [Required(ErrorMessage = "Bắt buộc")]
        [Range(1000, int.MaxValue, ErrorMessage = "Giá bán phải lớn hơn 1000")]
		[Column] public int GIABAN { get; set; }
        [Required(ErrorMessage = "Bắt buộc")]
        [Range(1, 500, ErrorMessage = "Số lượng tồn phải lớn hơn 1 và nhỏ hơn 500")]
		[Column] public int SOLUONGTON { get; set; }
		[Column] public int SOLUONGBAN { get; set; }
		[Column] public int LUOTXEM { get; set; }
		[Column] public int LUOTMUA { get; set; }
        [Required(ErrorMessage = "Bắt buộc")]
		[Column] public string MOTA { get; set; }
        [Required(ErrorMessage = "Bắt buộc")]
		[Column] public string HUONGDAN { get; set; }
        [Required(ErrorMessage = "Bắt buộc")]
		[Column] public DateTime NGAYDANG { get; set; }
        [Required(ErrorMessage = "Bắt buộc")]
		[Column] public string IMAGE { get; set; }
		[Column] public int LOAINANGLUONG { get; set; }
		[Column] public int LOAIDAY { get; set; }
		[Column] public int LOAIVO { get; set; }
		[Column] public bool COGIAMGIA { get; set; }
		[Column] public string NHASANXUAT { get; set; }
		[Column] public bool DAXOA { get; set; }
	}
    
	[TableName("dbo.HINHANH")]
	[PrimaryKey("MASANPHAM", AutoIncrement=false)]
	[ExplicitColumns]
    public partial class HINHANH : WatchShopConnectDB.Record<HINHANH>  
    {
		[Column] public string MASANPHAM { get; set; }
        [Required(ErrorMessage = "Bắt buộc")]
		[Column] public string TENHINHANH { get; set; }
	}
    
	[TableName("dbo.CHITIETDONHANG")]
	[PrimaryKey("MADONHANG", AutoIncrement=false)]
	[ExplicitColumns]
    public partial class CHITIETDONHANG : WatchShopConnectDB.Record<CHITIETDONHANG>  
    {
		[Column] public string MADONHANG { get; set; }
		[Column] public string MASANPHAM { get; set; }
		[Column] public int SOLUONG { get; set; }
		[Column] public int THANHTIEN { get; set; }
	}
    
	[TableName("dbo.BINHLUAN")]
	[PrimaryKey("STT")]
	[ExplicitColumns]
    public partial class BINHLUAN : WatchShopConnectDB.Record<BINHLUAN>  
    {
		[Column] public int STT { get; set; }
        [Required(ErrorMessage = "Bắt buộc")]
		[Column] public string NGUOIBINHLUAN { get; set; }
        [Required(ErrorMessage = "Bắt buộc")]
		[Column] public string THONGTINBINHLUAN { get; set; }
		[Column] public DateTime THOIGIAN { get; set; }
		[Column] public string MASANPHAM { get; set; }
		[Column] public bool DAXOA { get; set; }
	}
    
	[TableName("dbo.ViewUser")]
	[ExplicitColumns]
    public partial class ViewUser : WatchShopConnectDB.Record<ViewUser>  
    {
		[Column] public string UserName { get; set; }
		[Column] public string UserID { get; set; }
		[Column] public string HOTEN { get; set; }
		[Column] public DateTime NGAYSINH { get; set; }
		[Column] public string EMAIL { get; set; }
		[Column] public string SDT { get; set; }
		[Column] public string CMND { get; set; }
		[Column] public string DIACHI { get; set; }
		[Column] public bool DAXOA { get; set; }
		[Column] public string Name { get; set; }
	}
    
	[TableName("dbo.SP_VIEW")]
	[ExplicitColumns]
    public partial class SP_VIEW : WatchShopConnectDB.Record<SP_VIEW>  
    {
		[Column] public string MASANPHAM { get; set; }
		[Column] public string TENSANPHAM { get; set; }
		[Column] public string XUATXU { get; set; }
		[Column] public int GIAGOC { get; set; }
		[Column] public int GIABAN { get; set; }
		[Column] public int LUOTXEM { get; set; }
		[Column] public int LUOTMUA { get; set; }
		[Column] public string MOTA { get; set; }
		[Column] public string HUONGDAN { get; set; }
		[Column] public DateTime NGAYDANG { get; set; }
		[Column] public string IMAGE { get; set; }
		[Column] public string NHASANXUAT { get; set; }
		[Column] public bool DAXOA { get; set; }
		[Column] public string TENLOAI { get; set; }
		[Column] public string LOAIDAY { get; set; }
		[Column] public string LOAIVO { get; set; }
		[Column] public string TENNHASANXUAT { get; set; }
		[Column] public int IDNANGLUONG { get; set; }
		[Column] public int IDDAY { get; set; }
		[Column] public int IDVO { get; set; }
	}
    
	[TableName("dbo.sysdiagrams")]
	[PrimaryKey("diagram_id")]
	[ExplicitColumns]
    public partial class sysdiagram : WatchShopConnectDB.Record<sysdiagram>  
    {
		[Column] public string name { get; set; }
		[Column] public int principal_id { get; set; }
		[Column] public int diagram_id { get; set; }
		[Column] public int? version { get; set; }
		[Column] public byte[] definition { get; set; }
	}
    
	[TableName("dbo.LOAIVO")]
	[PrimaryKey("ID")]
	[ExplicitColumns]
    public partial class LOAIVO : WatchShopConnectDB.Record<LOAIVO>  
    {
		[Column] public int ID { get; set; }
		[Column] public string TENLOAI { get; set; }
		[Column] public bool DAXOA { get; set; }
	}
}



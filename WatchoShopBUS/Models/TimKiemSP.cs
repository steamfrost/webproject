﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WatchShopBUS.Models
{
    public class TimKiemSP
    {
        [DisplayName("Tên sản phẩm")]
        public string TenSanPham { get; set; }
        [DisplayName("Giá sản phẩm")]
        public int? beginGia { get; set; }
        public int? endGia { get; set; }
        [DisplayName("Nhà sản xuất")]
        public string NhaSanXuat { get; set; }
        [DisplayName("Loại năng lượng")]
        public int? LoaiNangLuong { get; set; }
        [DisplayName("Loại vỏ")]
        public int? LoaiVo { get; set; }
        [DisplayName("Loại dây")]
        public int? LoaiDay { get; set; }
    }
}
